import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AuroraComponent } from './aurora/aurora.component';
import { EltazkaraComponent } from './eltazkara/eltazkara.component';

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'aurora', component: AuroraComponent },
  { path: 'eltazkara', component: EltazkaraComponent }

];
