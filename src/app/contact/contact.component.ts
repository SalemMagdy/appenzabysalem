import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import CustomValidators from '../forms/CustomValidators';
import  { HttpModule } from '@angular/http';
import { FirstService } from './first.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact-component.css'],
  providers: [ FirstService ]
})
export class ContactComponent implements OnInit {

  persons=[];


 constructor (private _firstService : FirstService){}
  ngOnInit() {

     this._firstService.getResponsee()
         .subscribe(getPersonsData => this.persons = getPersonsData,
                                            err => console.log(err),
                                    () => console.log('Completed'));
         console.log("this.persons");
        
  }
  DoIt(value : any){
    console.log(value);
  }



}
