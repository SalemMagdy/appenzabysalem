import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
// import { ILocalStorageServiceConfig } from 'angular-2-local-storage';
import { HomeComponent } from './home/home.component';
import { AuroraComponent } from './aurora/aurora.component';
import { EltazkaraComponent } from './eltazkara/eltazkara.component';
// import { LocationStrategy, HashLocationStrategy } from '@angular/common';
// import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuroraComponent,
    EltazkaraComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: false })
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}
